# Aufgabenblatt: Umfassende Programmieraufgaben zu Dart

## Aufgabe 1: Typinferenz und Datentypen
**Beschreibung:** Erstelle Variablen mit `var` und `final` und weise ihnen initiale Werte zu. Überprüfe und erkläre, warum Dart den spezifischen Datentyp inferiert hat. Drucke die Typen und Werte dieser Variablen.

## Aufgabe 2: Objektorientierung
**Beschreibung:** Definiere eine Klasse `Auto` mit den Eigenschaften Marke, Modell und Baujahr. Implementiere eine Methode, die das Alter des Autos basierend auf dem aktuellen Jahr berechnet. Erstelle ein Objekt dieser Klasse und drucke das Alter des Autos.

## Aufgabe 3: Funktionale Programmierung
**Beschreibung:** Verwende `map` und `reduce` Methoden, um die Summe aller Zahlen in einer Liste zu berechnen, die durch 3 teilbar sind. Verwende dazu eine Liste von zufällig generierten Zahlen.

## Aufgabe 4: Asynchrone Programmierung
**Beschreibung:** Schreibe eine asynchrone Funktion, die zufällig entscheidet, ob eine Operation erfolgreich war oder nicht, und wirf eine Ausnahme im Fehlerfall. Verwende `try`, `catch` und `finally` zur Fehlerbehandlung und drucke das Ergebnis oder den Fehler.

## Aufgabe 5: Streams und Isolates
**Beschreibung:** Erstelle einen Stream, der fortlaufend Zahlen emittiert. Verbrauche diesen Stream in einem Isolate und drucke jede Zahl mit einer Verzögerung von einer Sekunde.

## Aufgabe 6: Metaprogrammierung
**Beschreibung:** Nutze die `mirrors` Bibliothek, um alle Methoden einer Klasse zu inspizieren, die du in einer früheren Aufgabe definiert hast. Drucke die Namen dieser Methoden.

## Aufgabe 7: Generics und Collections
**Beschreibung:** Erstelle eine generische Klasse `Box<T>`, die irgendeinen Inhalt speichern kann. Füge Methoden zum Speichern und Abrufen des Inhalts hinzu. Demonstriere die Nutzung dieser Box mit mindestens zwei verschiedenen Datentypen.

## Aufgabe 8: Netzwerkprogrammierung
**Beschreibung:** Schreibe ein Programm, das eine HTTP-Anfrage an eine API sendet, um Daten zu holen (nutze eine öffentliche API wie https://jsonplaceholder.typicode.com/posts). Verarbeite die Antwort und drucke einige spezifische Details aus den Daten.